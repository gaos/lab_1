#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 11:38:46 2019

@author: gabriel
"""
# 2

def vocales(letra):
# condiciones
    if(letra == 'a' or letra == 'A' ):
        return True
    if(letra == 'e' or letra == 'E' ):
        return True
    if(letra == 'i' or letra == 'I' ):
        return True
    if(letra == 'o' or letra == 'O' ):
        return True
    if(letra == 'u' or letra == 'U' ):
        return True
    else:
        return False
"""
try:
    letra = str(input('Ingrese una letra: '))
    if(vocales(letra) == True):
        print('Es vocal')
    else:
        print('Es consonante')
except ValueError:
    print("Error")
   """ 
def cuenta_vocales_y_consonantes(palabra):
    try:
        cont_vocal = 0
        cont_consonantes = 0
        
        for i in range(0, len(palabra)):
            
# compara y cuenta
            
            if(vocales(palabra[i]) == True):
                cont_vocal = cont_vocal + 1
            elif(vocales(palabra[i]) == False):
                cont_consonantes = cont_consonantes + 1
        print('Vocales',cont_vocal, 'Consonantes',cont_consonantes)
    except:
        pass

palabra = str(input('Ingrese una palabra: '))
cuenta_vocales_y_consonantes(palabra)
